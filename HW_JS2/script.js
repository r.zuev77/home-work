let flug = true;

// while (flug == true) {
//   let a = prompt("Enter number");

//   if (a == 10) {
//     alert("Right");
//     flug = false;
//   } else {
//     alert("Enter another number");
//     flug = true;
//   }
// }

// while (flug == true) {
//   let min = prompt("Enter number between 0 and 59");

//   if (min > 59 || min < 0) {
//     alert("Number out of range");
//     flug = true;
//   } else if (min > 0 && min < 15) {
//     alert("First quarter");
//     flug = false;
//   } else if (min >= 15 && min < 30) {
//     alert("Second quarter");
//     flug = false;
//   } else if (min >= 30 && min < 45) {
//     alert("Third quarter");
//     flug = false;
//   } else {
//     alert("Fourth quarter");
//     flug = false;
//   }
//}
let result;

while (flug == true) {
  let num = prompt("Enter number of season");
  if (num < 1 || num > 4) {
    alert("Season doesn't exist");
    flug = true;
  } else if (num == 1) {
    result = "It's winter";
    flug = false;
  } else if (num == 2) {
    result = "It's spring";
    flug = false;
  } else if (num == 3) {
    result = "It's summer";
    flug = false;
  } else {
    result = "It's fall";
    flug = false;
  }
}
alert(result);
